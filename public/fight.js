class fight extends Phaser.Scene{
    constructor(){
        super({key: "fight"});
    }
    preload(){
      this.load.html('hbuttons', 'hbuttons.html');
      this.load.html('dbuttons', 'dbuttons.html');
      this.load.html('pstats', 'pstats.html');
      //this.load.image('R', 'renderpix/0001.png');
      this.load.spritesheet('p1', 'renderpix/player1.png', { frameWidth: 100, frameHeight: 100 /*,endFrame: 6*/ });
      this.load.spritesheet('p2', 'renderpix/player2.png', { frameWidth: 100, frameHeight: 100 /*,endFrame: 6*/ });
      this.load.spritesheet('ready', 'renderpix/ready.png', { frameWidth: 225, frameHeight: 225 });

      this.load.spritesheet('hi', 'renderpix/HI.png', { frameWidth: 25, frameHeight: 75 });
      this.load.spritesheet('di', 'renderpix/DI.png', { frameWidth: 25, frameHeight: 75 });

      this.load.image('hit', 'renderpix/HIT.png');
      this.load.image('block', 'renderpix/BLOCK.png');

      this.load.spritesheet('youwin', 'renderpix/YOUWIN.png' ,{ frameWidth: 299, frameHeight: 52});
      this.load.spritesheet('youlose', 'renderpix/YOULOSE.png',{ frameWidth: 314, frameHeight: 52});

    }
    create(){




      this.player = this.add.sprite(w/2-50, h/2, 'p1');
      this.player.type="p";
      this.opponent = this.add.sprite(w/2+50, h/2, 'p2');
      this.opponent.flipX=true;
      this.opponent.visible=false;
      this.opponent.type="o";

      this.player.hinfo = this.add.sprite(w/2-100, h/2, 'hi');
      this.opponent.hinfo = this.add.sprite(w/2+100, h/2, 'hi');this.opponent.hinfo.flipX=true;

      this.player.dinfo = this.add.sprite(w/2-100, h/2, 'di');
      this.opponent.dinfo = this.add.sprite(w/2+100, h/2, 'di');

      this.player.hit = this.add.image(w/2-50,h/2-50,'hit');
      this.opponent.hit = this.add.image(w/2+50,h/2-50,'hit');

      this.player.block = this.add.image(w/2-50,h/2-50,'block');
      this.opponent.block = this.add.image(w/2+50,h/2-50,'block');

      this.player.youwin = this.add.sprite(w/2,h/2-150,'youwin');
      this.player.youwin.visible=false;
      this.player.youlose = this.add.sprite(w/2,h/2-150,'youlose');
      this.player.youlose.visible=false;



      var atk = 0;
      var def = 2;
      this.playersactions = [];

      this.players=[this.player,this.opponent];

      this.pstats = this.add.dom(50, 50).createFromCache('pstats');
      this.pstats.y = 50+this.pstats.height/2;
      this.ostats = this.add.dom(w-50, 50).createFromCache('pstats');
      this.ostats.y = 50+this.pstats.height/2;

      var hbuttons = this.add.dom(75/2, h-300/2).createFromCache('hbuttons');
      hbuttons.addListener('pointerdown');
      hbuttons.on('pointerdown', function (event) {
        if(event.target.localName!="button")return;
        var buttons = this.getChildByID('buttons')
        for (var i = 0; i < buttons.childNodes.length; i++) {
            if(buttons.childNodes[i].classList){
              buttons.childNodes[i].classList.remove("active");
            }
            if(buttons.childNodes[i].name == event.target.name){
              buttons.childNodes[i].classList.add("active");
              atk=parseInt(event.target.name);
              var token= localStorage.getItem('token');
              socket.emit("ready",JSON.stringify({token:token,cmd:{atk:atk,def:def}}) );
            }
        }
      });

      var dbuttons = this.add.dom(w-75/2, h-300/2).createFromCache('dbuttons');
      dbuttons.addListener('pointerdown');
      dbuttons.on('pointerdown', function (event) {
        if(event.target.localName!="button")return;
        var buttons = this.getChildByID('buttons')
        for (var i = 0; i < buttons.childNodes.length; i++) {
            if(buttons.childNodes[i].classList){
              buttons.childNodes[i].classList.remove("active");
            }
            if(buttons.childNodes[i].name == event.target.name){
              buttons.childNodes[i].classList.add("active");
              def=parseInt(event.target.name);
              var token= localStorage.getItem('token');
              socket.emit("ready",JSON.stringify({token:token,cmd:{atk:atk,def:def}}) );
              //player1.play(event.target.name);
            }
        }
      });

      this.anims.create({key: 'prest',frameRate: 10,
          frames: this.anims.generateFrameNumbers('p1', { start: 0, end: 0, first: 0 }),
          });
      this.anims.create({key: 'ph0',frameRate: 0.75,
          frames: this.anims.generateFrameNumbers('p1', { start: 1, end: 1, first: 1 }),
          });
      this.anims.create({key: 'ph1',frameRate: 0.75,
          frames: this.anims.generateFrameNumbers('p1', { start: 2, end: 2, first: 2 }),
          });
      this.anims.create({key: 'ph2',frameRate: 0.75,
          frames: this.anims.generateFrameNumbers('p1', { start: 3, end: 3, first: 3 }),
          });
      this.anims.create({key: 'pd0',frameRate: 0.75,
          frames: this.anims.generateFrameNumbers('p1', { start: 4, end: 4, first: 4 }),
          });
      this.anims.create({key: 'pd1',frameRate: 0.75,
          frames: this.anims.generateFrameNumbers('p1', { start: 5, end: 5, first: 5 }),
      });
      this.anims.create({key: 'pd2',frameRate: 0.75,
          frames: this.anims.generateFrameNumbers('p1', { start: 6, end: 6, first: 6 }),
      });
      this.anims.create({key: 'win',frameRate: 0.75,
          frames: this.anims.generateFrameNumbers('p1', { start: 7, end: 7, first: 7 }),
      });
      this.anims.create({key: 'lose',frameRate: 0.75,
          frames: this.anims.generateFrameNumbers('p1', { start: 8, end: 8, first: 8 }),
      });

      this.anims.create({key: 'orest',frameRate: 10,
          frames: this.anims.generateFrameNumbers('p2', { start: 0, end: 0, first: 0 }),
          });
      this.anims.create({key: 'oh0',frameRate: 0.75,
          frames: this.anims.generateFrameNumbers('p2', { start: 1, end: 1, first: 1 }),
          });
      this.anims.create({key: 'oh1',frameRate: 0.75,
          frames: this.anims.generateFrameNumbers('p2', { start: 2, end: 2, first: 2 }),
          });
      this.anims.create({key: 'oh2',frameRate: 0.75,
          frames: this.anims.generateFrameNumbers('p2', { start: 3, end: 3, first: 3 }),
          });
      this.anims.create({key: 'od0',frameRate: 0.75,
          frames: this.anims.generateFrameNumbers('p2', { start: 4, end: 4, first: 4 }),
          });
      this.anims.create({key: 'od1',frameRate: 0.75,
          frames: this.anims.generateFrameNumbers('p2', { start: 5, end: 5, first: 5 }),
      });
      this.anims.create({key: 'od2',frameRate: 0.75,
          frames: this.anims.generateFrameNumbers('p2', { start: 6, end: 6, first: 6 }),
      });
      this.anims.create({key: 'win',frameRate: 1,
          frames: this.anims.generateFrameNumbers('p2', { start: 7, end: 7, first: 7 }),
      });
      this.anims.create({key: 'lose',frameRate: 1,
          frames: this.anims.generateFrameNumbers('p2', { start: 8, end: 8, first: 8 }),
      });
      //this.add.image(w/2, h/2, 'R');

      this.anims.create({key: 'youwin',frameRate: 0.2,
          frames: this.anims.generateFrameNumbers('youwin', { start: 0, end: 0, first: 0 }),
      });
      this.anims.create({key: 'youlose',frameRate: 0.2,
          frames: this.anims.generateFrameNumbers('youwin', { start: 0, end: 0, first: 0 }),
      });

      this.player.youwin.on("animationcomplete",()=>{this.player.youwin.visible=false;
        this.opponent.visible=false})
      this.player.youwin.on("animationstart",()=>{this.player.youwin.visible=true})

      this.player.youlose.on("animationcomplete",()=>{this.player.youwin.visible=false;
        this.opponent.visible=false})
      this.player.youlose.on("animationstart",()=>{this.player.youwin.visible=true})

      //this.player.youwin.visible=false;




      this.action_button = this.add.sprite(w/2, h-300/2, 'ready', 0).setInteractive();
      //action_button.on('pointerover', function () {this.setFrame(1);});
      //action_button.on('pointerout', function () {this.setFrame(0);});
      this.action_button.on('pointerdown', function (event) {
        if(this.frame!=0){
            var token= localStorage.getItem('token');
            socket.emit("ready",JSON.stringify({token:token,cmd:{atk:atk,def:def}}) );
            this.setFrame(2);
        }
        //this.setFrame(2);
        });

      if(!socket.hasListeners("unlock")){
          socket.on("unlock",function(data){
            var fightscene = game.scene.getScene('fight');
            fightscene.action_button.setFrame(1);
          })
        }
      if(!socket.hasListeners("game")){
        socket.on("game",function(data){
          var jsondata = JSON.parse(data);
          console.log(jsondata);
          var fightscene = game.scene.getScene('fight');
          for(var i in jsondata.players){
            var player = jsondata.players[i];
            fightscene.playersactions.push(player);
          }
          if(jsondata.estado!="end"){
            fightscene.action_button.setFrame(2);
          }
          if(jsondata.estado=="end"){
            fightscene.action_button.setFrame(0);
          }
          //console.log(fightscene);
        })
      }
      if(!socket.hasListeners("player")){
        socket.on("player",function(data){
          //console.log(data);
          var fightscene = game.scene.getScene('fight');
          var jsondata = JSON.parse(data);
          fightscene.player.username = jsondata.username;
          fightscene.fillstats(fightscene.pstats,jsondata);
        })
      }
      var get_player = function(){
        var token= localStorage.getItem('token');
        socket.emit("get_player",JSON.stringify({token:token}));
      }
      get_player();

      this.fillstats=function(panel,player){

        //console.log(fightscene.pstats.getChildByID('username'));
        panel.getChildByID('username').innerHTML=player.username
        panel.getChildByID('rank').innerHTML=player.rank
        panel.getChildByID('life').innerHTML=player.life-player.damage+"/"+player.life
      }
      //setInterval(get_player,5000);*/



      this.runamins = function(){
          //console.log(this);
          var fightscene = game.scene.getScene('fight');

          //var rnd = Math.floor((Math.random() * 3));

          if( ( !fightscene.player.anims.isPlaying && !fightscene.opponent.anims.isPlaying)
            ){
              //console.log(fightscene.player.anims)
            //return;
            fightscene.resetanims();
            fightscene.resetposes();
            var player
            var opponent
            if(fightscene.playersactions.length>0){
              var action = fightscene.playersactions.shift();
              console.log(action);
              fightscene.players.forEach(function(pl){
                pl.visible = true;
                //DEFINE PLAYER POSITIONS
                if(fightscene.player.username == action.username && pl.type=="p"){
                  //MYSELF
                  pl.username = action.username
                  fightscene.fillstats(fightscene.pstats,action);
                  //console.log("Myself "+pl.username)
                }
                if(fightscene.player.username != action.username && pl.type=="o"){
                  //OTHERS
                  pl.username = action.username
                  fightscene.fillstats(fightscene.ostats,action);
                  //console.log("Others "+pl.username)
                  //player.visible = true;
                }

                //console.log(pl);
                //DEFINE DEFENSE
                if(pl.username == action.username){
                  player = pl;
                }
                //DEFINE ATTACKS
                for(var j in action.opponents){
                  var acop = action.opponents[j];
                  if(pl.username == acop.username){
                    opponent = pl;
                    if(acop.cmd && acop.cmd.atk!=undefined){
                      pl.play(pl.type+"h"+acop.cmd.atk)
                      pl.hinfo.visible=true;
                      pl.hinfo.setFrame(acop.cmd.atk);
                      }
                    else{
                      pl.play(pl.type+"rest")
                    }

                    console.log(acop);
                    /*if(acop.estado=="end" && fightscene.playersactions.length==0){
                      if( acop.damage<acop.life ){
                        pl.once( 'animationcomplete', () => {
                            fightscene.resetanims();
                            pl.anims.play("win")
                          })
                          if( pl.type=="p"){
                            pl.youwin.play("youwin")
                            }
                      }else{
                        pl.once( 'animationcomplete', () => {
                            fightscene.resetanims();
                            pl.anims.play("lose")
                          })
                          if( pl.type=="p"){
                            pl.youwin.play("youlose")
                            }
                      }
                    }*/

                  }
                }
              })

              //DEFENSE
              if(action.cmd && action.cmd.def!=undefined){
                player.dinfo.setFrame(action.cmd.def);
                if(action.blocksprite!=undefined){
                  player.dinfo.visible=true;
                  player.play(player.type+"d"+action.blocksprite)

                  //END GAME
                  /*if(action.estado=="end" && fightscene.playersactions.length==0){
                    if( action.damage>=action.life ){
                      player.once( 'animationcomplete', () => {
                          fightscene.resetanims();
                          player.anims.play("lose")

                        })
                        if( player.type=="p"){
                          player.youlose.play("youwin")
                        }
                    }else{
                      player.once( 'animationcomplete', () => {
                          fightscene.resetanims();
                          player.anims.play("win")

                        })
                        if( player.type=="p"){
                          player.youlose.play("youlose")
                        }
                    }
                  }*/

                  //.on('animationcomplete', () => {this.player.anims.play("right", true)
                }
                }
              else{
                player.play(player.type+"rest")
              }
              if(action.hitinfo=="hit"){
                player.hit.visible=true;
              }
              if(action.hitinfo=="block"){
                player.block.visible=true;
              }


            }//else{fightscene.resetanims()}
          }//else{fightscene.resetanims();}
          };

          this.resetposes=function(){
            var fightscene = game.scene.getScene('fight');
            fightscene.players.forEach(function(pl){
              pl.play(pl.type+"rest")
            })
          }
          this.resetanims=function(){
            var fightscene = game.scene.getScene('fight');
            fightscene.players.forEach(function(pl){
              //pl.play(pl.type+"rest")
              pl.dinfo.visible=false;
              pl.hinfo.visible=false;
              pl.hit.visible=false;
              pl.block.visible=false;
              //if(pl.youwin) pl.youwin.visible=false;
              //if(pl.youlose) pl.youlose.visible=false;
            })
          }
          this.resetanims();

          var timer = this.time.addEvent({
              delay: 100,                // ms
              callback: this.runamins,
              //args: [],
              //callbackScope: thisArg,
              loop: true
          });

    }
}
