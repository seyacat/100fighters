class forms extends Phaser.Scene{
    constructor(){
        super({key: "forms"});
    }
    preload(){
      this.load.html('loginform', 'loginform.html');
      this.load.html('logoutform', 'logoutform.html');
    }
    create(){
      //this.add.text(0, 0, 'Hello World', { fontFamily: '"Roboto Condensed"' });
      var loginform = this.add.dom(w/2, h/2).createFromCache('loginform');
      var logoutform = this.add.dom(40, 40).createFromCache('logoutform');

      logoutform.visible = false;
      loginform.visible = false;
      //loginform.setPerspective(800);

      loginform.addListener('click');
      loginform.on('click', function (event) {
      if (event.target.name === 'loginButton')
      {
          var inputUsername = this.getChildByName('username');
          var inputPassword = this.getChildByName('password');
          if (inputUsername.value !== '' && inputPassword.value !== '')
          {
              socket.emit('login',JSON.stringify({username:inputUsername.value,password:inputPassword.value}))
          }
      }
      });

      logoutform.addListener('click');
      logoutform.on('click', function (event) {
      if (event.target.name === 'logoutButton')
      {
              socket.emit('logout',"");
      }
      });
      if(socket.connected){
        console.log("connected");
        var token= localStorage.getItem('token');
        socket.emit("test_login",JSON.stringify({token:token}));
      };
      socket.on("token",function(data){
        console.log(data);
        var jsondata = JSON.parse(data);
        if(jsondata && jsondata.token){ //IS LOGGED
          loginform.visible=false;
          logoutform.visible=true;
          localStorage.setItem('token', jsondata.token);

          game.scene.getScene('default').bg.y = h/2-300
          if(!game.scene.isActive('fight')){
            game.scene.start('fight');
          }

        }else{ //NOT LOGGED
          loginform.visible=true;
          logoutform.visible=false;
          localStorage.removeItem('token');

          game.scene.getScene('default').bg.y = h/2
          game.scene.stop('fight');
        }
      });
      this.scene.bringToTop();
    }
}
