_log = console.log;
global.console.log = function() {
	var traceobj = new Error("").stack.split("\n")[2].split(":");
	var file = traceobj[0].split(process.env.PWD + '/')[1];
	var line = traceobj[1];
	var new_args = [file+" "+line + " >>"];
	new_args.push.apply(new_args, arguments);
	_log.apply(null, new_args);
};

var express = require('express');
var app = express();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var md5 = require('md5');
var _ = require('lodash');

    app.use('/', express.static('public'));
    app.use('/phaser/', express.static('node_modules/phaser'));
    //app.use('/socket.io/', express.static('node_modules/socket.io-client'));
    players = [
			{username:"noob2",type:"bot",life:100,damage:0,rank:102,cmds:[{atk:2,def:2}]},
			{username:"noob1",type:"bot",life:100,damage:0,rank:101,cmds:[{atk:1,def:1}]},
			{username:"noob",type:"bot",life:100,damage:0,rank:100,cmds:[{atk:0,def:0}]},
			{username:"tabo",type:"bot",life:100,damage:0,rank:99,cmds:[{atk:0,def:0},{atk:1,def:1}]},
			{username:"niko",type:"bot",life:100,damage:0,rank:98,cmds:[{atk:0,def:0},{atk:2,def:2}]},
		];
		games=[];

    io.on('connection', function(socket){
        console.log('a user connected');
        socket.on('login',function(data){
          var jsondata = JSON.parse(data);
          console.log(jsondata)
          if(!jsondata.username || !jsondata.password) return;
          const now = new Date()
          const secondsSinceEpoch = Math.round(now.getTime() / 1000)
          var token = md5(jsondata.username+jsondata.password+secondsSinceEpoch);
          var player = _.find(players, function(o) { return o.username == jsondata.username; });
          if(!player){//registrar
            console.log("register success");
            player = {username:jsondata.username,
							password:md5(jsondata.username+jsondata.password),
							token:token,
							rank:100,
							life:100,
							damage:0,
							socket:socket.id
						}
            players.push(player)
            socket.player = player;
            socket.emit("token",JSON.stringify({token:token}));
          }else{//ya registrado
            if(player.password == md5(jsondata.username+jsondata.password)){
              console.log("login success");
              player.token=token;
							player.socket=socket.id;
              socket.player = player;
              socket.emit("token",JSON.stringify({token:token}));
            }else{
              console.log("login fail");
            }
          }
          console.log("login");

        })
        socket.on('test_login',function(data){
					if(!testlogin(socket,data)){return;}
        })
				socket.on('get_player',function(data){
					console.log("get_player");
					if(!testlogin(socket,data)){return;}
					socket.emit("player",JSON.stringify(socket.player))
        })

        socket.on('logout',function(data){
          console.log("logout");
          console.log(socket.player);
          socket.player = null;
          socket.emit("token",null)
        })

				socket.on('ready',function(data){
					console.log(data)
					if(!testlogin(socket,data))return;
					jsondata=JSON.parse(data);
					if(socket.player.estado!="game"){
						socket.player.estado="ready";
					}
					if(jsondata.cmd){
						socket.player.cmd=jsondata.cmd;
					}
					//var jsondata = JSON.parse(data);
					//FIND OPONENT
					//var oponent = _.find(players, function(o) { return o.username == jsondata.username; });

				})
      });

loop = function(){
	for(scs in  io.sockets.sockets ){
		var sc=io.sockets.sockets[scs]
		if(!sc.player)continue;


		if(sc.player.estado=="ready" && sc.player.retry && sc.player.retry>5 && sc.player.lastgame=="lose"){
			//lower lavel
			var opponent = _.find(players, function(o) { return o.username != sc.player.username
				&& o.estado!="game"
				&& o.rank-1 == sc.player.rank
				; });
			if(opponent){
				sc.player.retry=0
				//prizes
				delete sc.player.lastgame
				sc.player.gametype="lower";
				sc.player.prizes={win:sc.player.rank,lose:sc.player.rank+1};
				opponent.gametype="upper";
				opponent.prizes={win:opponent.rank-1,lose:opponent.rank};
				pair([sc.player,opponent]);}
		}
		if(sc.player.estado=="ready" && sc.player.retry && sc.player.retry>5){
			//same lavel
			var opponent = _.find(players, function(o) { return o.username != sc.player.username
				&& o.estado!="game"
				&& o.rank == sc.player.rank
				; });
			if(opponent){
				sc.player.retry=0
				//prizes
				delete sc.player.lastgame
				sc.player.gametype="same";
				sc.player.prizes={win:sc.player.rank,lose:sc.player.rank+1};
				opponent.gametype="same";
				opponent.prizes={win:opponent.rank,lose:opponent.rank+1};
				pair([sc.player,opponent]);}
		}
		if(sc.player.estado=="ready" && sc.player.retry && sc.player.retry>5 && sc.player.lastgame!="lose"){
			//one level up
			var opponent = _.find(players, function(o) { return o.username != sc.player.username
				&& o.estado!="game"
				&& o.rank+1 == sc.player.rank
				; });
			if(opponent){
				sc.player.retry=0
				//prizes
				delete sc.player.lastgame
				sc.player.gametype="upper";
				sc.player.prizes={win:sc.player.rank-1,lose:sc.player.rank};
				opponent.gametype="lower";
				opponent.prizes={win:opponent.rank,lose:opponent.rank+1};
				pair([sc.player,opponent]);}
		}

		if(sc.player.estado=="ready" ){
			if(!sc.player.retry){sc.player.retry=1}
			else{sc.player.retry++}
		}
	}
}
setInterval(loop,1000);

playersloop = function(){
	players.forEach(function(player){
		if(player.damage>0){player.damage--;}
		var sc=io.sockets.sockets[player.socket]
		if(sc){
			sc.emit("player",JSON.stringify(sc.player));
		}
	})
}
setInterval(playersloop,5000);

pair=function(players){
	players.forEach(function(player){
		//delete player.cmd;
		player.estado="game";
	})
	games.push({players:players});
	sendgame({players:players});
	console.log(JSON.stringify(games));
}
gameloop = function(){
	games.forEach(function(game){
		if(game.tic==undefined){game.tic=0;}
		if(game.turn==undefined){game.turn=0;}
		game.tic++
		//
		//if(game)
		//console.log(JSON.stringify(game));
		//var cflag = false;
		game.players.forEach(function(player){
			/*if(!player.cmd && io.sockets.sockets[player.socket] ){
				//player connected
				//console.log(player.username+" connected")
				//cflag = false;
			}*/
			if(player.cmds && !io.sockets.sockets[player.socket]){
				//player not connected
				//console.log(player.username+" not connected")
				player.cmd = player.cmds[game.turn%player.cmds.length];
			}
		})
		if(game.tic==1){
			sendunlock(game);
		}
		if( game.tic==3){
			//game.tic=0;
			processgame(game);
			postprocessgame(game);
			game.turn++;
		}
		if( game.tic>=6){
			game.tic=0;
		}
	})
}
setInterval(gameloop,1000);

postprocessgame = function(game){
	if(game.estado=="end"){
		game.players.forEach(function(player){
			for(var i in player.opponents){
				player.opponents[i].estado="end"
			}
			if(player.lastgame=="lose"){
				player.rank=player.prizes.lose;
			}else{
				player.rank=player.prizes.win;
			}
			player.estado = "end"
		})
		sendgame(game);
		//DELETE GAME
		const gameindex = games.indexOf(game);
		if (gameindex > -1) {
		  games.splice(gameindex, 1);
		}
	}
	else{
		sendgame(game);
	}
}
processgame = function(game){
	game.players = _.shuffle(game.players);
	for(i in game.players){
		var player = game.players[i]
		player.opponents = [];
		for(j in game.players){
			var opponent = game.players[j];
			if(player.username == opponent.username)continue;
			player.opponents.push(cleanplayer(opponent));
			//resolve
			delete player.blocksprite;
			if(player.health==undefined){player.life=100}
			if(player.damage==undefined){player.damage=0}
			if(!player.cmd || player.cmd.def==undefined){player.damage+=10;console.log("def undefined")}
			if(!opponent.cmd || opponent.cmd.atk==undefined){continue;console.log("atk undefined")}
			if(player.cmd==undefined || player.cmd.def==undefined || opponent.cmd.atk == player.cmd.def){
				player.damage+=200
				player.hitinfo="hit";
				console.log("hit atk-"+opponent.username+" "+opponent.cmd.atk+" def "+player.username+" "+player.cmd.def)
				player.blocksprite=(opponent.cmd.atk+1)%3;
				}
			else{
				player.hitinfo="block";
				player.blocksprite=opponent.cmd.atk;
			}
		}
			if(player.damage>=player.life){
				player.damage=player.life
				player.lastgame="lose";
				game.estado="end";
				break;
			}
		}
		//console.log(game)
}
sendunlock= function(game){
	game.players.forEach(function(player){
		var socket = io.sockets.sockets[player.socket];
		if(socket){
			socket.emit("unlock","");
		}
	})
}
sendgame = function(game){
	console.log("sendgame")
	game.players.forEach(function(player){
		var socket = io.sockets.sockets[player.socket];
		if(socket){
				//console.log("emitgame")
				socket.emit("game",JSON.stringify(cleangame(game)));
		}
		//delete player.cmd
	})
}
cleangame = function(game){
	retgame = _.cloneDeep(game)
	retgame.players.forEach(function(player){
		player=cleanplayer(player)
	})
	return retgame
	}
cleanplayer = function(player){
	retplayer = _.cloneDeep(player)
	delete retplayer.password
	delete retplayer.cmds
	delete retplayer.socket
	delete retplayer.token
	return retplayer
}

testlogin = function(socket,data){
	var jsondata = JSON.parse(data);
	if(socket.player){
		if(jsondata.token == socket.player.token){
			socket.player.socket=socket.id;
			socket.emit("token",data)
			return true;
		}else{
		socket.emit("token",null);
		return false;
		}
	}else{
		var player = _.find(players, function(o) { return o.token == jsondata.token; });
		if(player){//logged
			socket.player=player;
			player.socket=socket.id;
			socket.emit("token",data)
			return true;
		}else{//fail test
			socket.emit("token",null)
			return false;
		}
	}
}

http.listen(8080, function(){
  console.log('listening on *:8080');
});
